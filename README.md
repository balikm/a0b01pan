Důležité informace o předmětu 

**Přednášející:   [Jan Hamhalter](http://math.feld.cvut.cz/hamhalte)  a  [Veronika Sobotíková](http://math.feld.cvut.cz/veronika)**

###Zápočet a zkouška

Předmět je zakončen standardně zápočtem a zkouškou. Podmínkou pro získání zápočtu je aktivní účast na výuce. Hodnocení předmětu bude záviset na zkoušce samotné. Zkouška je ústní a je při ní 
zkoušena probraná látka.

[Obecné informace o předmětu na fakultních stránkách (anotace, osnova, literatura, požadavky)](http://www.fel.cvut.cz/cz/education/bk_peo/predmety/18/30/p1830206)